# Leerstandsmelder

Website um Informationen über (Immobilien-)Leerstand zu sammeln und darzustellen.

## Entwicklungsumgebung aufsetzen
1. Projekt clonen, und in das Verzeichnis wechseln.
2. Virtualenv aufsetzen: `python3 -m venv venv`
3. Virtualenv aktivieren: `source venv/bin/activate`
4. Requirements installieren: `pip install -r requirements.txt`
5. App ausführen: `flask run` oder `python app.py`