from model_views.base_model_view import BaseModelView
from flask_security.utils import hash_password


class UserModelView(BaseModelView):
    form_columns = ('username', 'password', 'email', 'active', 'roles')

    def on_model_change(self, form, model, is_created):
        # If creating a new user, hash password
        if is_created:
            model.password = hash_password(form.password.data)
        else:
            old_password = form.password.object_data
            # If password has been changed, hash password
            if not old_password == model.password:
                model.password = hash_password(form.password.data)
