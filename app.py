from flask import Flask, render_template, flash, redirect, url_for, request, jsonify
from flask_hcaptcha import hCaptcha
from flask_security import Security, SQLAlchemyUserDatastore, login_required, current_user
from flask_security.utils import hash_password
from flask_admin import Admin
from flask_admin.menu import MenuLink

from forms.vacancy_change_proposal_form import VacancyChangeProposalForm
from forms.vacancy_form import VacancyForm
from forms.vacancy_moderation_form import VacancyModerationForm
from forms.complaint_form import ComplaintForm
from config import Config
import logging
from logging.handlers import RotatingFileHandler
import os
import requests
from urllib.parse import quote
from datetime import datetime
from PIL import Image

from models.base import db
from models.vacancies import Vacancy, Ownership, BuildingType, Degree
from models.users import User
from models.roles import Role
from models.vacancy_change_proposals import VacancyChangeProposal

from model_views.base_model_view import BaseModelView
from model_views.user_model_view import UserModelView

if not os.path.exists('logs'):
    os.mkdir('logs')
file_handler = RotatingFileHandler('logs/leerstandsmelder.log', maxBytes=102400,
                                   backupCount=3)
file_handler.setFormatter(logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
file_handler.setLevel(logging.INFO)

app = Flask(__name__)
app.config.from_object(Config)
app.logger.addHandler(file_handler)

app.logger.setLevel(logging.DEBUG)
app.logger.info('Leerstandsmelder is starting...')

# Init db
db.init_app(app)

app.config['HCAPTCHA_ENABLED'] = True
app.config['HCAPTCHA_ENABLED'] = True

# Init hCaptcha
hcaptcha = hCaptcha(app)
hcaptcha.secret_key = app.config['HCAPTCHA_SECRET_KEY']
hcaptcha.site_key = app.config['HCAPTCHA_SITE_KEY']

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

# Init Flask-Admin
admin = Admin(app, name='Leerstandsmelder Admin Area', template_mode='bootstrap3')
admin.add_view(UserModelView(User, db.session))
admin.add_view(BaseModelView(Role, db.session))
admin.add_view(BaseModelView(Vacancy, db.session))
admin.add_view(BaseModelView(VacancyChangeProposal, db.session))
admin.add_link(MenuLink(name='Leerstandsmelder', category='', url='/'))


@app.before_first_request
def setup_roles():
    user_datastore.find_or_create_role(name='admin', description='Administratoren dürfen alles.')
    user_datastore.find_or_create_role(name='moderator', description='Moderatoren können Einträge freischalten und '
                                                                     'bearbeiten')
    testuser = User.query.filter_by(email='lasse@az-aachen.de').one_or_none()
    if not testuser:
        user_datastore.create_user(email='lasse@az-aachen.de', password=hash_password('password'), active=True,
                                   username='lasse', roles=['admin', 'moderator'])
        db.session.commit()

    grotesk = User.query.filter_by(email='grotesk@soliteko.de').one_or_none()
    if not grotesk:
        user_datastore.create_user(email='grotesk@soliteko.de', password=hash_password('password'), active=True,
                                   username='grotesk', roles=['moderator'])
        db.session.commit()


with app.app_context():
    db.create_all()

with app.app_context():
    if Vacancy.query.count() < 1:
        muffi = Vacancy(longitude=6.067199, latitude=50.775707, name="Muffi", street="Muffeter Weg", number="5",
                        plz=52074, city="Aachen", owner="BLB NRW AC", ownership=Ownership.PUBLIC, since="2013",
                        degree=Degree.FULL, building_type=BuildingType.FLAT, submission_date=datetime.now(),
                        active=True)
        db.session.add(muffi)
        polp = Vacancy(longitude=6.100856, latitude=50.797952, name="Altes Polizeipräsidium",
                       street="Hubert-Wienen-Str.", number="21-25", plz=52070, city="Aachen", active=True)
        db.session.add(polp)
        db.session.commit()


@app.route('/')
def index():
    vacancies = Vacancy.query.filter_by(active=True).all()
    return render_template('map.html', vacancies=vacancies)


@app.route('/about')
def get_about_page():
    return render_template('info.html')


@app.route('/vacancy', methods=['GET', 'POST'])
def add_vacancy():
    form = VacancyForm()
    if request.method == "POST":
        if form.validate_on_submit():
            if current_user.is_authenticated or hcaptcha.verify():
                # Create new vacancy
                # Get latitude and longitude
                try:
                    lat, lon = get_coordinates_from_address(street=form.street.data, number=form.number.data,
                                                            city=form.city.data, postal=form.postal.data)
                except IndexError:
                    app.logger.error(f"No results for address {form.street.data} {form.number.data}, "
                                     f"{form.postal.data} {form.city.data}.")
                    flash("Diese Adresse scheint nicht zu existieren!")
                    return render_template('add_vacancy.html', form=form)

                img = None
                app.logger.debug(f"Image: {form.image}")
                if form.image.data:
                    try:
                        img = Image.open(form.image.data)
                        app.logger.debug(f"Someone uploaded an image of size {img.size}!")
                        img.thumbnail((400, 400))
                    except OSError as e:
                        app.logger.error(f"Error reading file: {e}")

                new_vacancy = Vacancy(name=form.name.data, since=form.since.data,
                                      ownership=Ownership(form.ownership.data),
                                      building_type=BuildingType(form.building_type.data),
                                      degree=Degree(form.degree.data), owner=form.owner.data,
                                      description=form.description.data, longitude=lon, latitude=lat,
                                      street=form.street.data, number=form.number.data,
                                      plz=form.postal.data, city=form.city.data)
                db.session.add(new_vacancy)
                db.session.commit()
                if img:
                    app.logger.debug(f"Saving uploaded and reduced image to {new_vacancy.id}.jpg")
                    img.save(os.path.join(app.root_path, f'static/img/{new_vacancy.id}.jpg'), progressive=True)
                app.logger.info(f"Creating new (inactive) vacancy: {new_vacancy}")
                flash("Neuer Leerstand erfolgreich eingereicht. Ein Moderator wird diesen bald freischalten!")
                return redirect(url_for('index'))
            else:
                flash("Oh nein, du bist ein Roboter!")
                print("Captcha failed!")
                return render_template('add_vacancy.html', form=form)
        else:
            return render_template('add_vacancy.html', form=form)
    else:
        return render_template('add_vacancy.html', form=form)


@app.route('/vacancy/<id>')
def get_vacancy_details(id):
    vacancy = Vacancy.query.get(id)
    has_img = os.path.isfile(f'static/img/{vacancy.id}.jpg')
    result_dict = {
        'name': vacancy.name,
        'body': render_template('modal_body.html', vacancy=vacancy, has_img=has_img)
    }
    return jsonify(result_dict)


@app.route('/moderation', methods=['GET', 'POST'])
@login_required
def moderation():
    form = VacancyModerationForm()

    if request.method == 'POST':

        # If delete button was clicked
        if form.delete.data:
            # Delete entry
            unmoderated_vacancy = Vacancy.query.get(form.id.data)
            db.session.delete(unmoderated_vacancy)
            db.session.commit()

        # If submit was clicked and form has no errors
        elif form.validate_on_submit():
            # Update data which may have been changed
            unmoderated_vacancy = Vacancy.query.get(form.id.data)

            unmoderated_vacancy.longitude = form.lon.data
            unmoderated_vacancy.latitude = form.lat.data
            unmoderated_vacancy.name = form.name.data
            unmoderated_vacancy.street = form.street.data
            unmoderated_vacancy.number = form.number.data
            unmoderated_vacancy.plz = form.postal.data
            unmoderated_vacancy.city = form.city.data
            unmoderated_vacancy.owner = form.owner.data
            unmoderated_vacancy.ownership = Ownership(form.ownership.data)
            unmoderated_vacancy.since = form.since.data
            unmoderated_vacancy.building_type = BuildingType(form.building_type.data)
            unmoderated_vacancy.degree = Degree(form.degree.data)
            unmoderated_vacancy.description = form.description.data
            unmoderated_vacancy.active = True
            unmoderated_vacancy.submission_date = datetime.now()
            db.session.commit()
            flash("Leerstand erfolgreich freigeschaltet.")

        # If form had validation errors:
        else:
            # Return form with errors
            return render_template('moderate_vacancy.html', form=form)

    # Get first inactive entry to moderate
    vacancies = Vacancy.query.filter_by(active=False).all()

    # If no entries to moderate, return to map
    if not vacancies:
        flash("Keine neuen Leerstände!")
        return redirect(url_for('index'))

    forms = []

    for unmoderated_vacancy in vacancies:
        # Return pre-filled form to make corrections
        form = VacancyModerationForm(id=unmoderated_vacancy.id, name=unmoderated_vacancy.name,
                                     since=unmoderated_vacancy.since, street=unmoderated_vacancy.street,
                                     number=unmoderated_vacancy.number, city=unmoderated_vacancy.city,
                                     postal=unmoderated_vacancy.plz, lat=unmoderated_vacancy.latitude,
                                     lon=unmoderated_vacancy.longitude, building_type=unmoderated_vacancy.building_type,
                                     degree=unmoderated_vacancy.degree, ownership=unmoderated_vacancy.ownership,
                                     owner=unmoderated_vacancy.owner, description=unmoderated_vacancy.description)
        forms.append(form)

    return render_template('moderate_vacancy.html', forms=forms)


@app.route('/correction/<id>', methods=['GET', 'POST'])
def correction(id):
    vacancy = Vacancy.query.get(id)

    form = VacancyChangeProposalForm()

    if request.method == 'POST':

        # If submit was clicked and form has no errors
        if form.validate_on_submit():
            if current_user.is_authenticated or hcaptcha.verify():
                # Update data which may have been changed
                # TODO: Update coordinates if address has changed
                vacancy.name = form.name.data
                vacancy.street = form.street.data
                vacancy.number = form.number.data
                vacancy.plz = form.postal.data
                vacancy.city = form.city.data
                vacancy.owner = form.owner.data
                vacancy.ownership = Ownership(form.ownership.data)
                vacancy.since = form.since.data
                vacancy.building_type = BuildingType(form.building_type.data)
                vacancy.degree = Degree(form.degree.data)
                vacancy.description = form.description.data
                vacancy.active = True
                vacancy.submission_date = datetime.now()
                db.session.commit()
                flash("Änderungsvorschlag erfolgreich eingereicht.")

                vacancies = Vacancy.query.filter_by(active=True).all()
                return render_template('map.html', vacancies=vacancies)

            else:
                flash("Oh nein, du bist ein Roboter!")
                print("Captcha failed!")
                return render_template('change_vacancy.html.html', form=form, vacancy=vacancy)

    form = VacancyChangeProposalForm(vacancy_id=vacancy.id, name=vacancy.name, since=vacancy.since,
                                     street=vacancy.street, number=vacancy.number, city=vacancy.city,
                                     postal=vacancy.plz, lat=vacancy.latitude, lon=vacancy.longitude,
                                     building_type=vacancy.building_type, degree=vacancy.degree,
                                     ownership=vacancy.ownership, owner=vacancy.owner, description=vacancy.description)

    return render_template('change_vacancy.html', form=form, vacancy=vacancy)


def get_coordinates_from_address(street, number, city, postal, country='DE'):
    search_str = quote(f"{street} {number} {postal} {city}")
    response = requests.get(f"https://api.mapbox.com/geocoding/v5/mapbox.places/{search_str}.json?country={country}&"
                            f"access_token={app.config['MAPBOX_ACCESS_TOKEN']}")
    search_results = response.json()

    lon = search_results['features'][0]['center'][0]
    lat = search_results['features'][0]['center'][1]

    return lat, lon


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
