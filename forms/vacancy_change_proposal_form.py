from wtforms import HiddenField
from wtforms.validators import DataRequired
from forms.vacancy_form import VacancyForm


class VacancyChangeProposalForm(VacancyForm):
    vacancy_id = HiddenField(DataRequired())
