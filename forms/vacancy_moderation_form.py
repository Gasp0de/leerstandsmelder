# coding=utf-8
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, TextAreaField, FloatField, HiddenField
from wtforms.validators import DataRequired, Length, Regexp
from models.vacancies import BuildingType, Degree, Ownership


class VacancyModerationForm(FlaskForm):
    id = HiddenField('vacancy_id')

    name = StringField('Titel', validators=[DataRequired("Ein Titel muss angegben werden!"),
                                            Length(min=5, max=50,
                                                   message="Der Titel muss zwischen 5 und 50 Zeichen lang sein!")])

    since = StringField('Leer seit', validators=[Length(max=50, message="Bitte nicht mehr als 50 Zeichen angeben.")],
                        render_kw={"placeholder": "Ca. 3 Jahre"})

    street = StringField('Strasse',
                         validators=[DataRequired(message="Strasse und Hausnummer müssen ausgefüllt werden!"),
                                     Regexp(r'^[a-zA-ZÄÖÜäöüß .\-]+$', message='Bitte Strasse und Hausnummer '
                                                                               'angeben!')],
                         render_kw={"placeholder": "Muffeter Weg"})
    number = StringField('Hausnummer', validators=[DataRequired(message="Die Hausnummer muss ausgefüllt werden!"),
                                         Regexp(r'^\d+[a-z]?$', message="Die Hausnummer darf nur aus Zahlen "
                                                                        "und maximal einem folgenden "
                                                                        "Buchstaben bestehen."),
                                         Length(max=5)], render_kw={"placeholder": "5"})

    city = StringField('Stadt', validators=[DataRequired(message='Stadt muss ausgefüllt werden!'),
                                            Length(min=3, max=30)], render_kw={"placeholder": "Aachen"})

    postal = StringField('Postleitzahl', validators=[DataRequired(message='Bitte Postleitzahl angeben!')],
                         render_kw={"placeholder": "52074"})

    lat = FloatField('Längengrad', validators=[DataRequired(message='Der Längengrad ist notwendig.')])

    lon = FloatField('Breitengrad', validators=[DataRequired(message='Der Breitengrad ist notwendig')])

    building_type = SelectField('Art der Immobilie', choices=BuildingType.choices(),
                                validators=[DataRequired()])

    degree = SelectField('Grad des Leerstands', choices=Degree.choices(), validators=[DataRequired()])

    ownership = SelectField('Art des Eigentümers', choices=Ownership.choices())

    owner = StringField('Eigentümer', validators=[Length(max=50,
                                                         message="Der Eigentümer darf maximal 50 Zeichen lang sein.")],
                        render_kw={"placeholder": "Bau und Liegenschaftsbetrieb, Vonovia, Landmarken AG, etc."})

    description = TextAreaField('Beschreibung', validators=[Length(max=300,
                                                                   message="Die Beschreibung darf maximal 300 Zeichen "
                                                                           "lang sein!")])

    delete = SubmitField('Vorschlag löschen')

    submit = SubmitField('Speichern')
