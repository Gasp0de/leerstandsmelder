# coding=utf-8
from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, HiddenField, StringField
from wtforms.validators import DataRequired, Length, Email


class ComplaintForm(FlaskForm):
    id = HiddenField('id', validators=[DataRequired("Dieser Leerstand existiert nicht!")])
    text = TextAreaField('Beschwerdetext', validators=[DataRequired("Sie müssen einen Beschwerdetext angeben!"),
                                                       Length(min=5, max=2000,
                                                              message="Ihre Beschwerde ist länger als 2000 Zeichen, "
                                                                      "bitte schreiben sie uns eine Email!")])
    email = StringField('E-Mail (freiwillig)', validators=[Email("Bitte geben sie eine gültige E-Mail Adresse ein!")])
    submit = SubmitField('Abschicken', render_kw={"data-dissmiss": "modal"})
