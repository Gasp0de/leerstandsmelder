from sqlalchemy.orm import relationship

from models.base import db
import enum


class BuildingType(enum.Enum):
    FLAT = "Wohnraum"
    BUSINESS = "Gewerbe"
    MIXED = "Gemischt"
    OTHER = "Anderes"
    UNKNOWN = "Unbekannt"

    def __str__(self):
        return self.value

    @classmethod
    def choices(cls):
        return [(choice, choice.value) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return item if isinstance(item, BuildingType) else BuildingType[item]


class Degree(enum.Enum):
    FULL = "Vollständig"
    PARTIALLY = "Teilweise"
    UNKNOWN = "Unbekannt"

    def __str__(self):
        return self.value

    @classmethod
    def choices(cls):
        return [(choice, choice.value) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return item if isinstance(item, Degree) else Degree[item]


class Ownership(enum.Enum):
    PRIVATE = "Privat"
    COMPANY = "Firma"
    CHURCH = "Kirche"
    PUBLIC = "Öffentliche Institution"
    UNKNOWN = "Unbekannt"

    def __str__(self):
        return self.value

    @classmethod
    def choices(cls):
        return [(choice, choice.value) for choice in cls]

    @classmethod
    def coerce(cls, item):
        return item if isinstance(item, Ownership) else Ownership[item]


class Vacancy(db.Model):
    __tablename__ = 'vacancies'
    id = db.Column(db.Integer, primary_key=True)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    name = db.Column(db.String(64), nullable=False)
    street = db.Column(db.String(64))
    number = db.Column(db.String(10))
    plz = db.Column(db.Integer)
    city = db.Column(db.String(25))
    owner = db.Column(db.String(64))
    ownership = db.Column(db.Enum(Ownership))
    since = db.Column(db.String(64))
    building_type = db.Column(db.Enum(BuildingType))
    degree = db.Column(db.Enum(Degree))
    description = db.Column(db.String)
    submission_date = db.Column(db.DateTime)
    active = db.Column(db.Boolean, default=False)
    change_proposals = relationship("VacancyChangeProposal", backref="vacancy")

    def __repr__(self):
        return f"{self.name}, {self.street} {self.number}, {self.plz} {self.city}, Lat: {self.latitude} " \
               f"Lon: {self.longitude}"
