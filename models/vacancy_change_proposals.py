from sqlalchemy import ForeignKey

from models.base import db
from models.vacancies import Ownership, BuildingType, Degree


class VacancyChangeProposal(db.Model):
    __tablename__ = 'vacancy_change_proposals'
    id = db.Column(db.Integer, primary_key=True)
    vacancy_id = db.Column(db.Integer, ForeignKey("vacancies.id"))
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    name = db.Column(db.String(64), nullable=False)
    street = db.Column(db.String(64))
    number = db.Column(db.String(10))
    plz = db.Column(db.Integer)
    city = db.Column(db.String(25))
    owner = db.Column(db.String(64))
    ownership = db.Column(db.Enum(Ownership))
    since = db.Column(db.String(64))
    building_type = db.Column(db.Enum(BuildingType))
    degree = db.Column(db.Enum(Degree))
    description = db.Column(db.String)
    submission_date = db.Column(db.DateTime)
    active = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"{self.name}, {self.street} {self.number}, {self.plz} {self.city}, Lat: {self.latitude} " \
               f"Lon: {self.longitude}"
